const express = require("express");
const router = express.Router();
const users = require("../model/users");
const bcrypt = require("bcryptjs");
const emailValidation = require("../functions/emailValidation");
const passwordValidation = require("../functions/passwordValidation");
const jwt = require("jsonwebtoken");
const JWT_SECRET_KEY = "4356jhefs8u5*&&&*hjh3wsgetet2424ftgh434s";

//Register endpoint
router.post("/register", async (req, res) => {
  const { name, email, password: plainTextPassword } = req.body;

  if (!name || typeof name !== "string") {
    return res.json({ status: "error", message: "Name should be string" });
  }

  if (emailValidation(email) === false) {
    return res.json({ status: "error", message: "Invalid email address" });
  }

  if (passwordValidation(plainTextPassword) === false) {
    return res.json({
      status: "error",
      message: "Password should be atleast 8 characters",
    });
  }

  const password = await bcrypt.hash(plainTextPassword, 10);
  const token = await jwt.sign(
    {
      name: name,
      email: email,
    },
    JWT_SECRET_KEY
  );

  try {
    const response = await users.create({
      name,
      email,
      password,
    });
    console.log("user created successfully", response);
  } catch (error) {
    if (error.code === 11000) {
      return res.json({ status: "error", message: "Email already registered" });
    }
    throw error;
  }
  res.json({
    status: "OK",
    data: { name: name, email: email, access_token: token },
  });
});

//Login endpoint
router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  const getUser = await users.findOne({ email }).lean();

  if (!getUser) {
    return res.json({
      status: "error",
      message: "Incorrect email or password",
    });
  }

  const token = await jwt.sign(
    {
      name: getUser.name,
      email: email,
    },
    JWT_SECRET_KEY
  );

  if (await bcrypt.compare(password, getUser.password)) {
    return res.json({
      status: "OK",
      data: { name: getUser.name, email: getUser.email, access_token: token },
    });
  }
  res.json({ status: "error", message: "Incorrect email or password" });
});

module.exports = router;
