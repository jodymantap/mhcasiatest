const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const JWT_SECRET_KEY = "4356jhefs8u5*&&&*hjh3wsgetet2424ftgh434s";
const axios = require("axios");

//Get crypto endpoint
router.get("/getAll", async (req, res) => {
  const { access_token } = req.body;

  try {
    const verified = await jwt.verify(access_token, JWT_SECRET_KEY);
    axios
      .get(
        "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=df1acc6b-31c8-4b9a-8d83-034fe6307496"
      )
      .then((response) => {
        if (verified) {
          res.json(response.data);
        } else {
          res.json({ status: "error", message: "You are not logged in yet." });
        }
      });
  } catch {
    res.json({ status: "error", message: "You are not logged in yet." });
  }
});

module.exports = router;
