const express = require("express");
const path = require("path");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const dbURI =
  "mongodb://jodymantap:jodymantap@cluster0-shard-00-00.gv6fn.mongodb.net:27017,cluster0-shard-00-01.gv6fn.mongodb.net:27017,cluster0-shard-00-02.gv6fn.mongodb.net:27017/mhcasia?ssl=true&replicaSet=atlas-d3185j-shard-0&authSource=admin&retryWrites=true&w=majority";
mongoose
  .connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
  .then((result) => console.log("Connected to DB"))
  .catch((err) => console.log(err));


app.use('/api', require("./routes/users"));
app.use('/api/crypto', require("./routes/crypto"));


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
