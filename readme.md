--- Overview ---

On this Backend API, I use mongoDB atlas so everyone do not need to import it to local mongoDB to try the appplication.



--- How to install ---

* Clone this repository on your local computer.

![Cloning](screenshots/clone.png)


* Open it on your code editor.

![Code](screenshots/code.png)


(Make sure all the dependencies are installed by saying "npm install" on the terminal).


* Open up the terminal and say "npm run dev" to run the application.

![Running](screenshots/running.png)


Now you are ready to test each of the endpoints from the postman collection.



--- How to test ---

* Start with importing the API collection to your postman application. The collection JSON file is on this repository on api collection/MHC Test API Collection.postman_collection.json.

![Import](screenshots/import.png)


* Open the collection. It has two folders for GET REQUEST and POST REQUEST.

![Collections](screenshots/collections.png)


* Test each of the endpoints provided. You can change the data on the body and run it again to test with another data.

![Test](screenshots/test.png)


Thank you!


